from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

# -----------Local Imports-----------#
from django.urls import reverse_lazy
from tasks.models import Task


# --------Task Views-------- #
class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create_task.html"
    fields = ["name", "assignee", "start_date", "due_date", "project"]
    success_url = reverse_lazy("show_project project.pk")

    def get_success_url(self):
        return reverse_lazy("show_my_tasks")


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/tasks_list.html"
    context_object_name = "tasks"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "<int:pk>/complete.html"
    fields = ["is_completed"]
    success_url = "/tasks/mine/"
