from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView


urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("", RedirectView.as_view(url="/projects/"), name="home"),
    path("accounts/", include("accounts.urls")),
    path("tasks/", include("tasks.urls")),
]
