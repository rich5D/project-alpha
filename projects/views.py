from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.urls import reverse_lazy

# -------Local Imports-------#
from projects.models import Project


# --------Task Views-------- #
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/projectlist.html"
    context_object_name = "projects"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/project_details.html"
    context_object_name = "project"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tasks"] = self.object.tasks.all()
        return context


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/new_project.html"
    fields = ["name", "description", "members"]
    success_url = reverse_lazy("show_project")

    def get_success_url(self):
        return reverse_lazy("show_project", kwargs={"pk": self.object.pk})
